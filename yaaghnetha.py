def read_input_file(filename):
    students = {}
    with open(filename, 'r') as file:
        lines = file.readlines()
    for line in lines:
        data = line.strip().split()

        student_name = data[0]
        marks = list(map(int, data[1:]))
        students[student_name] = marks
    return students

def compare_students(marks1, marks2):
    return all(m1 > m2 for m1, m2 in zip(marks1, marks2))

def relation(students):
    student_names = list(students.keys())
    n = len(student_names)
    relations = []
    visited = set()

    for i in range(n):
        if student_names[i] in visited:
            continue
        current_chain = [student_names[i]]
        visited.add(student_names[i])
        for j in range(n):
            if i != j and student_names[j] not in visited:
                if compare_students( students[student_names[i]], students[student_names[j]] ):
                    current_chain.append( student_names[j] )
                    visited.add( student_names[j] )
                elif compare_students( students[student_names[j]], students[student_names[i]] ):
                    current_chain.insert(0, student_names[j])
                    visited.add(student_names[j])
        relations.append(current_chain)
    
    return relations

def print_relations(relations):
    for relation in relations:
        if len(relation) > 1:
            print(' > '.join(relation))

def main():
    filename = 'studentRanking.txt'
    students = read_input_file(filename)
    relations = relation(students)
    print_relations(relations)

if __name__ == "__main__":
    main()
