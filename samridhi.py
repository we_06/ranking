SPACE = " "
LARGE = 1000

def parse(line: str) -> list:
    name, *marks = line.strip().split(SPACE)
    marks = [int(_) for _ in marks]
    return [name] + marks + [sum(marks)]

def load(mark_file: str) -> list:
    return [parse(line) for line in open(mark_file)]

def compare_marks(one: list, two: list) -> bool:
    return all(one[i+1] > two[i+1] for i in range(len(one) - 1))

def arrange(mark_list: list) -> list:
    def make_key(one: list) -> tuple:
        return (LARGE - int(one[-1]), one[0])
    
    sorted_list = sorted(mark_list, key=make_key)
    
    final_list = []
    for i in range(len(sorted_list)):
        placed = False
        for j in range(i):
            if compare_marks(sorted_list[i], sorted_list[j]):
                final_list.insert(j, sorted_list[i])
                placed = True
                break
        if not placed:
            final_list.append(sorted_list[i])
    
    return final_list

def assign_pos(score_wise: list) -> list:
    rank = 0
    assigned = []
    for position, one_score in enumerate(score_wise, start=1):
        rank = position
        assigned.append(f'{rank:4} {one_score[0]:40} {one_score[-1]:5}')
    return assigned


sorted_marks = arrange(load("marks.txt"))
ranks_list = assign_pos(sorted_marks)
for student in ranks_list:
    print(student)